<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aula 14</title>
</head>
<body>
    <h1>Aula 14 - PHP OO </h1>
    <hr>
    <h3>Alunos: Davi Viana Oliveira Rodrigues - Turma: aut2d3</h3>
    <hr>
    <?php
        require_once 'usuario_class.php';
        $usuario1 = new Usuario('Davi Viana Oliveira Rodrigues','104.018.526.60','Rua 2 de outubro, 85');

        echo $usuario1->nome;
        echo '<br>';
        echo $usuario1->getCpf();
        echo '<br>';
        echo $usuario1->getEndereco();
        echo '<br>';

        echo '<hr>';

        $usuario2 = new Usuario('Maria julia','222.213.225-22','Rua das camélias, 415');

        echo $usuario2->nome;
        echo '<br>';
        echo $usuario2->getCpf();
        echo '<br>';
        echo $usuario2->getEndereco();
        echo '<br>';

        echo '<hr>';

        $usuario3 = new Usuario('Maria ana','222.213.225-56','Rua das acacias, 415');

        echo $usuario3->nome;
        echo '<br>';
        echo $usuario3->getCpf();
        echo '<br>';
        echo $usuario3->getEndereco();
        echo '<br>';

        echo '<hr>';
    ?>
</body>
</html>
